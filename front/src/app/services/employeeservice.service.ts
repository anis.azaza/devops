import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeserviceService {
  host = "http://localhost:8080/apiemp";

  constructor(private client: HttpClient) { }

  getAllEmployees(): Observable<Employee[]> {
    return this.client.get<Employee[]>(this.host+"/all")
  }
  getEmployeeById(id: any): Observable<Employee> {
    return this.client.get<Employee>(this.host +"/emp/"+ id);
  }
  public addEmployee(employee: Employee): Observable<void> {
    return this.client.post<void>(this.host+"/add1/", employee);
  }
  public deleteEmployee(id:any):Observable<Employee>{
    return this.client.delete<Employee>(this.host+"/delete/"+id)
  }

}
