import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../model/product';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductserviceService {
  host = "http://localhost:8081/produitapi";

  constructor(private client: HttpClient) { }

  getAllProducts(): Observable<Product[]> {
    return this.client.get<Product[]>(this.host+"/all")
  }
  getProductById(id: any): Observable<Product> {
    return this.client.get<Product>(this.host +"/product/"+ id);
  }
  public addProduct(produit: Product): Observable<void> {
    return this.client.post<void>(this.host+"/ajouter/", produit);
  }
  public deleteProduct(id:any):Observable<Product>{
    return this.client.delete<Product>(this.host+"/delete/"+id)
  }
}
