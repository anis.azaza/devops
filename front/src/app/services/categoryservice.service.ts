import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Categorie } from '../model/categorie';


@Injectable({
  providedIn: 'root'
})
export class CategoryserviceService {
  host ="http://localhost:8081/categorieapi";

  constructor(private client :HttpClient) {}
    getAllCategories():Observable<Categorie[]>{
      return this.client.get<Categorie[]>(this.host+"/all")
    } 
    
  }

