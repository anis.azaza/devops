import {Categorie} from "./categorie"
export class Product {
  id!:number;
  nom!:string;
  prix!:number;
  quantite!:number;
  imageName!:string;
  categorie!:Categorie;

}
