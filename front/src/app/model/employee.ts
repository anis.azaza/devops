import { Departement } from "./departement"
import { Skill } from "./skill"
export class Employee{
    id!:number
    fname!:string
    lname!:string
    email!:string
    age!:number
    departement!:Departement
    skils!:Skill[]
} 