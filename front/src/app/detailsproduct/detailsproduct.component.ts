import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../model/product';
import { ProductserviceService } from '../services/productservice.service';

@Component({
  selector: 'app-detailsproduct',
  templateUrl: './detailsproduct.component.html',
  styleUrls: ['./detailsproduct.component.css']
})
export class DetailsproductComponent {
  product!:Product;

  constructor(private ar:ActivatedRoute, private sp:ProductserviceService,private router:Router){}

  ngOnInit():void{
    let id:number=this.ar.snapshot.params['id'];
    console.log(id);
    this.sp.getProductById(id).subscribe(data=>{this.product=data
      console.log("Data",data)
    });
    }

    retourHome(){
      this.router.navigate(["liste"])
    }

}
