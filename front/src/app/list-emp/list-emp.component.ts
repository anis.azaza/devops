import { Component } from '@angular/core';
import Swal from 'sweetalert2';
import { Employee } from '../model/employee';
import { EmployeeserviceService } from '../services/employeeservice.service';

@Component({
  selector: 'app-list-emp',
  templateUrl: './list-emp.component.html',
  styleUrls: ['./list-emp.component.css']
})
export class ListEmpComponent {
  employees!: Employee[];
  employeefind!:Employee[];
  
  filtrer(txt:string){
    return this.employees.filter(element=>element.fname.indexOf(txt)!=-1)
  }
  set x(a:string){
    this.employees=this.filtrer(a);
  }
  

  constructor(private empservice:EmployeeserviceService){}
  ngOnInit():void{
  this.reloadData() 
  }
  deleteEmployee(id: number) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })
    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.empservice.deleteEmployee(id).subscribe(()=>this.reloadData())
        swalWithBootstrapButtons.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
      } else if (
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
    }
  
  reloadData():void{
    this.empservice.getAllEmployees().subscribe(data=>{
      this.employees=data
      this.employeefind=data
  })

}

}
