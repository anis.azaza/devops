import { identifierName } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsEmpComponent } from './details-emp/details-emp.component';
import { ListEmpComponent } from './list-emp/list-emp.component';
import { NotfoundComponent } from './notfound/notfound.component';

const routes: Routes = [
  {path:"listeemp", component:ListEmpComponent},
  {path:"listeemp/:id",component:DetailsEmpComponent},
  {path:"**",component:NotfoundComponent},
  {path:"",redirectTo:"/listeemp",pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
