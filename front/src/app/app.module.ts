import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { DetailsproductComponent } from './detailsproduct/detailsproduct.component';
import { ListEmpComponent } from './list-emp/list-emp.component';
import { DetailsEmpComponent } from './details-emp/details-emp.component';
import { AddEmpComponent } from './add-emp/add-emp.component';

@NgModule({
  declarations: [
    AppComponent,
    NotfoundComponent,
    DetailsproductComponent,
    ListEmpComponent,
    DetailsEmpComponent,
    AddEmpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
