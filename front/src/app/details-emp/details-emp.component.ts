import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../model/employee';
import { EmployeeserviceService } from '../services/employeeservice.service';

@Component({
  selector: 'app-details-emp',
  templateUrl: './details-emp.component.html',
  styleUrls: ['./details-emp.component.css']
})
export class DetailsEmpComponent {
  employee!:Employee;

  constructor(private ar:ActivatedRoute, private empservice:EmployeeserviceService,private router:Router){}

  ngOnInit():void{
    let id:number=this.ar.snapshot.params['id'];
    console.log(id);
    this.empservice.getEmployeeById(id).subscribe(data=>{this.employee=data
      console.log("Data",data)
    });
    }

    retourHome(){
      this.router.navigate(["listeemp"])
    }



}
