package com.FormationSpringBoot.GestionProduit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.FormationSpringBoot.GestionProduit.entities.Skills;


@Repository
public interface SkillsRepository extends JpaRepository<Skills, Integer>{

}
