package com.FormationSpringBoot.GestionProduit.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Employee;


@Repository
public interface DepartementRepository extends JpaRepository<Departement, Integer> {
	@Query("SELECT u FROM Departement u WHERE u.supervisor.id = ?1")
	Departement findDepBySup(int id);
	

}
