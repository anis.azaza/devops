package com.FormationSpringBoot.GestionProduit.controlleur;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.FormationSpringBoot.GestionProduit.dao.DepartementRepository;
import com.FormationSpringBoot.GestionProduit.dao.SkillsRepository;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.service.IServiceDepartement;
import com.FormationSpringBoot.GestionProduit.service.IServiceEmployee;
import com.FormationSpringBoot.GestionProduit.service.IServiceSkills;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/employee")
@AllArgsConstructor
public class EmployeeController {
	IServiceEmployee se;
	IServiceSkills ss;
	IServiceDepartement sd;
	SkillsRepository sr;
	DepartementRepository dr;
	
	@GetMapping("/delete/{id}")
	public String deleteEmployee(@PathVariable int id ) {
		se.deleteEmployee(id);
		return "redirect:/employee/all";
	}
	
	@GetMapping("/add")
	public String addEmployee(Model m) {
		m.addAttribute("departement",sd.allDepartements());
		m.addAttribute("skill",ss.getAllSkills());
		return "addemployee";
	}
	@PostMapping("/save")
	public String saveEmployee (@ModelAttribute Employee emp,Model m )throws IOException  {
		Integer id =emp.getId();
		se.saveEmployee(emp);
		if(id!=null) {
			return "redirect:/employee/all";
		}
		else
		{
			m.addAttribute("message","Successfully Added");
			m.addAttribute("skills",ss.getAllSkills());
			m.addAttribute("departements",sd.allDepartements());
			return "addemployee";
		}
	
	}
	@GetMapping("/update/{id}")
	public String modifierProduit(Model m,@PathVariable("id") int id) {
		m.addAttribute("skill",ss.getAllSkills());
		m.addAttribute("departement",sd.allDepartements());
		m.addAttribute("employee",se.getEmployee(id));
		return "addemployee";
		
	}
	
	@GetMapping("/employeesbydepartements")
	public String getEmployeesByDepartements(@RequestParam("departement") int iddep,Model m)
	{
		m.addAttribute("departement",sd.allDepartements());
		if (iddep==0) {System.out.println("ok");
			m.addAttribute("employee",se.getAllEmployees());
			return"redirect:/employee/all";}
			else {
				m.addAttribute("employee",sd.getEmployeeByDepartement(iddep));
				m.addAttribute("departement",dr.findById(iddep).get().getName());
				return "employee";
			}
		}
	@GetMapping("/all")
	public String getAllEmployees(Model m)
	{
		List<Employee>liste=se.getAllEmployees();
		m.addAttribute("employee",liste);
		m.addAttribute("departements",dr.findAll());
		m.addAttribute("departement","all departments");
		return "employee";
	}
	
	

}
