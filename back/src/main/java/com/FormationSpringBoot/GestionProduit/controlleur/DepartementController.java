package com.FormationSpringBoot.GestionProduit.controlleur;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.FormationSpringBoot.GestionProduit.dao.EmployeeRepository;
import com.FormationSpringBoot.GestionProduit.dao.SupervisorRepository;
import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.service.IServiceDepartement;
import com.FormationSpringBoot.GestionProduit.service.IServiceEmployee;
import com.FormationSpringBoot.GestionProduit.service.IServiceSupervisor;

import lombok.AllArgsConstructor;

@Controller
@RequestMapping("/departement")
@AllArgsConstructor
public class DepartementController {
	
	IServiceDepartement sd;
	IServiceSupervisor ss;
	IServiceEmployee se;
	SupervisorRepository sr;
	EmployeeRepository er;
	
	
	@GetMapping("/adddepartement")
	public String addDepartement(Model m) {
		m.addAttribute("supervisor",ss.getAllSupervisor());
		m.addAttribute("employees",se.getAllEmployees());
		return "adddepartement";
	}
	
	@PostMapping("/savedepartement")
	public String saveDepartement (@ModelAttribute Departement dep,Model m)  {
		Integer id =dep.getId();
		sd.addDepartement(dep);
		if(id!=null) {
			return "redirect:/departement/all";
		}
		else
		{
			m.addAttribute("message","Successufully Added");
			m.addAttribute("departement",ss.getAllSupervisor());
			m.addAttribute("departement",se.getAllEmployees());
			return "savedepartement";
		}
	}
	
	
	
	@GetMapping("/all")
	public String getAllDapertement(Model m)
	{
		List<Departement>list=sd.allDepartements();
		m.addAttribute("departement", list);
		m.addAttribute("supervisor", sr.findAll());
		m.addAttribute("employees", er.findAll());
		m.addAttribute("supervisor", "all supervisors");
		m.addAttribute("employees", "all employees");
		return "departements";
		
	}
	
	

}
