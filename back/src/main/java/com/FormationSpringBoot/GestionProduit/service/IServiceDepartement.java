package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;


public interface IServiceDepartement {
	public void addDepartement(Departement dep);
	public List<Departement> allDepartements();
	public List<Employee> getEmployeeByDepartement(int iddep);
	public Supervisor getSupervisorByDepartement(int iddep);
	public Departement getDepBySupervisor(int iddep); 

}
