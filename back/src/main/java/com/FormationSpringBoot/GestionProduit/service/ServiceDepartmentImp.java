package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FormationSpringBoot.GestionProduit.dao.CategorieRepository;
import com.FormationSpringBoot.GestionProduit.dao.DepartementRepository;
import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Employee;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;

import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class ServiceDepartmentImp implements IServiceDepartement{
	@Autowired
	DepartementRepository dr;

	@Override
	public void addDepartement(Departement dep) {
		
		dr.save(dep);
	}

	@Override
	public List<Departement> allDepartements() {
		
		return dr.findAll();
	}

	@Override
	public List<Employee> getEmployeeByDepartement(int iddep) {
		// TODO Auto-generated method stub
		return dr.getById(iddep).getListemp();
	}

	@Override
	public Supervisor getSupervisorByDepartement(int iddep) {
		// TODO Auto-generated method stub
		return dr.getById(iddep).getSupervisor();
	}

	@Override
	public Departement getDepBySupervisor(int iddep) {
		// TODO Auto-generated method stub
		return dr.findDepBySup(iddep);
	}

	
	
	

}
