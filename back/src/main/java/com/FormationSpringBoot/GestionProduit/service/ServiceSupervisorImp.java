package com.FormationSpringBoot.GestionProduit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FormationSpringBoot.GestionProduit.dao.CategorieRepository;
import com.FormationSpringBoot.GestionProduit.dao.SupervisorRepository;
import com.FormationSpringBoot.GestionProduit.entities.Departement;
import com.FormationSpringBoot.GestionProduit.entities.Supervisor;

import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class ServiceSupervisorImp implements IServiceSupervisor {

	@Autowired
	SupervisorRepository supr;
	@Override
	public void saveSupervisor(Supervisor supervisor) {
		// TODO Auto-generated method stub
		supr.save(supervisor);
		
	}

	@Override
	public List<Supervisor> getAllSupervisor() {
		// TODO Auto-generated method stub
		return supr.findAll();
	}

	@Override
	public Departement getDepartementBySupervisor(int idsup) {
		// TODO Auto-generated method stub
		return supr.getById(idsup).getDepartement();
	}

	
	
}
